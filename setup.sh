#!/bin/bash
composer update
sudo chown -R www-data:www-data /var/www/laravel/*
sudo chgrp -R www-data /var/www/laravel/storage /var/www/laravel/bootstrap/cache
sudo chmod -R ug+rwx /var/www/laravel/storage /var/www/laravel/bootstrap/cache
sudo find /var/www/laravel -type f -exec chmod 644 {} \;   
sudo find /var/www/laravel -type d -exec chmod 755 {} \;



